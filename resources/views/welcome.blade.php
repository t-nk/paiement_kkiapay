<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="utf-8">
      <title>AnGO</title>
      <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


      <!-- bootstrap -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <!-- line awesome -->
      <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">

      <!-- <script src="https://cdn.kkiapay.me/k.js"></script> -->

      <style>
          body {
              font-family: 'Nunito', sans-serif;
          }
      </style>
    </head>

    <body class="antialiased">
      <div class="container-fluid">

      <div class=" ">
        <nav class="navbar navbar-light bg-light">
          <div class="container-fluid">
            <img src="logo.jpg" alt="Girl in a jacket" width="100" height="50" class="d-inline-block align-text-top">
            <a class="navbar-brand" href="#">
              <font class="col-3 text-sm-center" size="-5">
              Votre visibilté sur le web c'est notre affaire !!!
              </font>
            </a>
          </div>
        </nav>
      </div>

      <div class="d-flex justify-content-center mt-4">
        <div class="card col-lg-5 col-sm-12">
          <div class="card-body">
            <div>
                Ce pack comprend la création, l'optimisation, la notoriété et la sécurisation de votre profil Google my business. Un service clé en main pour votre visibilité sur le web
            </div>

            <div class="d-flex justify-content-lg-end justify-content-sm-center justify-content-xs-center text-xs-center text-sm-center">
              <div class="text-white m-1 p-2 rounded bg-primary">
                <strong>459.99 CAD/TTC</strong>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="d-flex justify-content-center mt-2">
        <div class="card col-lg-5 col-sm-12">
          <div class="card-body">
            <form>
              <div> <h2> <strong>Informations sur le client !</strong></h2></div>
              <div class="row mt-3 gy-2">
                <div class="form-group col-lg-6 col-sm-12 col-xs-12">
                  <label for="exampleInputEmail1">Nom</label>
                  <input type="text" class="form-control" placeholder="" id="lastname" >
                </div>
                <div class="form-group col-lg-6 col-sm-12 col-xs-12">
                  <label for="exampleInputEmail1">Prénom(s)</label>
                  <input type="text" class="form-control" placeholder="" id="firstname" >
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Numéro </label>
                <input type="text" class="form-control" placeholder=""  id="number" >
              </div>
              <div class="row">
                <div class="form-group col-lg-6 col-sm-12 col-xs-12">
                  <label for="exampleInputEmail1">Nom Entreprise</label>
                  <input type="text" class="form-control" placeholder=""  id="enterprisename" >
                </div>
                <div class="form-group col-lg-6 col-sm-12 col-xs-12">
                  <label for="exampleInputEmail1">Adresse Entreprise</label>
                  <input type="text" class="form-control"  placeholder=""  id="enterpriseadress" >
                </div>
              </div>

              <div class="form-group mt-5" id="envoyer">
                <input type="submit" id="submit" class="form-control bg-primary text-white" style=""
                value="Passer à la caisse" onClick="save(this.form)">
              </div>
            </form>

            <div class="mt-3" id="smart-button-container"  style="">
              <div style="text-align: center;">
                <div id="paypal-button-container"></div>
                <!-- <div id="paypal-button"></div> -->
              </div>
            </div>

          </div>
        </div>
      </div>

      <!-- <div class="d-flex justify-content-center">
        <kkiapay-widget  amount="10" key="07eef161724a673aceb4d8d2b2f34e31271e75f5" callback="https://kkiapay-redirect.com" />
      </div> -->

      <br><br><br>

      <!-- Include the PayPal JavaScript SDK; replace "test" with your own sandbox Business account app client ID -->
      <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=USD"></script>

      <script>
        document.getElementById("envoyer").style.display = "block";
        document.getElementById("smart-button-container").style.display = "none";

        function save(form) {
          event.preventDefault();
          if(form.firstname.value !== "" &&  form.lastname.value !== "" &&  form.number.value !== "" &&
              form.enterprisename.value !== "" &&   form.enterpriseadress.value !== "" ) {
            document.getElementById("envoyer").style.display = "none";
            document.getElementById("smart-button-container").style.display = "block";
          }else{
            alert ("Veuillez bien remplir tous les champs");
          }
        }

        paypal.Buttons({
          // Sets up the transaction when a payment button is clicked
          createOrder: function(data, actions) {
            return actions.order.create({
              purchase_units: [{
                amount: {
                  value: '459.99'
                }
              }]
            });
          },
          // Finalize the transaction after payer approval
          onApprove: function(data, actions) {
            return actions.order.capture().then(function(orderData) {
              // Successful capture! For dev/demo purposes:
                  console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                  var transaction = orderData.purchase_units[0].payments.captures[0];
                  alert('Transaction '+ transaction.status + ': ' + transaction.id + '\n\nSee console for all available details');
            });
          },
          onError: function () {
                return (err) => {
                    console.error(err);
                    window.location.href = "/your-error-page-here";
                }
            },
         onShippingChange: function () {
                return (data, actions) => {

                }
            },
            onCancel: function (e) {
              console.log(e);
              alert('Paiement non effectué');
            },
        }).render('#paypal-button-container');
      </script>







      <!-- old script sb-->
      <!-- <script src="https://www.paypal.com/sdk/js?client-id=sb&enable-funding=venmo&currency=CAD" data-sdk-integration-source="button-factory"></script>
      <script>
        document.getElementById("envoyer").style.display = "block";
        document.getElementById("smart-button-container").style.display = "none";
        initPayPalButton();

        function save(form) {
          event.preventDefault();
          if(form.firstname.value !== "" &&  form.lastname.value !== "" &&  form.number.value !== "" &&
              form.enterprisename.value !== "" &&   form.enterpriseadress.value !== "" ) {
            document.getElementById("envoyer").style.display = "none";
            document.getElementById("smart-button-container").style.display = "block";
          }else{
            alert ("Veuillez bien remplir tous les champs");
          }
        }


        function initPayPalButton() {
          paypal.Buttons({
            style: {
              shape: 'pill',
              color: 'gold',
              layout: 'vertical',
              label: 'pay',
            },

            createOrder: function(data, actions) {
              return actions.order.create({
                purchase_units: [{"amount":{"currency_code":"CAD","value":459.99 }}]
              });
            },

            onApprove: function(data, actions) {
              return actions.order.capture().then(function(orderData) {

                // Full available details
                console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

                // Show a success message within this page, e.g.
                const element = document.getElementById('paypal-button-container');
                element.innerHTML = '';
                element.innerHTML = '<h3>Thank you for your payment!</h3>';

                // Or go to another URL:  actions.redirect('thank_you.html');

              });
            },

            onError: function(err) {
              console.log(err);
            }
          }).render('#paypal-button-container');
        }

      </script> -->

      <!-- kkia script -->
      <!-- <script amount="100"
        callback=""
        data=""
        url="https://www.mndlocks.com/wp-content/uploads/2021/04/picto_blanc.png"
        position="center"
        theme="#0095ff"
        sandbox="true"
        key="6543fb906e3c11ec9f5205a1aca9c042"
        src="https://cdn.kkiapay.me/k.js">
      </script> -->

      </div>


    </body>
</html>
